// let a = 10;
// const b = 10;
//
// let lst = [1, 2, 3, "awe"];
// let dict = {
//     name: "loki",
//     age: 21
// };
//
// // && - and
// // || - or
// // ! - not
//
// // 10 == "10" -> true
// // 10 === "10" -> false
// // None === null
//
// if (a < 10) {
//
// } else if (a < b) {
//
// } else {
//
// }
//
// switch (a + b) {
//     case 10:
//         console.log(123);
//         break;
//     case 20:
//         console.log('qwe');
//         break;
// }
//
// for (let i=0; i<lst.length; i++) {
//     console.log(lst[i]);
// }
//
// function some_func(x, y) {
//     return x + y;
// }
// some_func(2, 2);
//
// let my_mega_func = function (x, y) {
//     return x + y;
// };
// my_mega_func(2, 2);
//
//
// let some_user = {
//     name: 'user1',
//     money: 200,
//
//     for_the_greatest_good: function () {
//         this.money -= 10;
//     }
// };
//
// some_user.for_the_greatest_good();
// console.log(some_user.money);

// console.log(document);
// console.log(window);
let p_items = document.getElementsByTagName('p');

for (let i=0; i<p_items.length; i++) {
    p_items[i].innerHTML = '<b>Hello world</b>';
}