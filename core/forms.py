import re
from django import forms

from core.models import Brand, Product, Tag


# class ProductCreateForm(forms.Form):
#     brand = forms.ModelChoiceField(queryset=Brand.objects.all())
#     description = forms.CharField(widget=forms.widgets.Textarea())
#     run_km = forms.IntegerField()
#     condition = forms.ChoiceField(choices=Product.CONDITION_CHOICES)
#     number_plate = forms.CharField()
#     tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all())
#
#     def clean_number_plate(self):
#         reg = r'^\w{2}\d{4}\w{2}$'
#         if not re.match(reg, self.cleaned_data['number_plate']):
#             raise forms.ValidationError('Are you a monkey? Enter correct one!')
#         return self.cleaned_data['number_plate']
#
#     def save(self):
#         product = Product.objects.create(
#             brand=self.cleaned_data['brand'],
#             description=self.cleaned_data['description'],
#             run_km=self.cleaned_data['run_km'],
#             condition=self.cleaned_data['condition'],
#             number_plate=self.cleaned_data['number_plate']
#         )
#         product.tags.add(*self.cleaned_data['tags'])

class ProductCreateForm(forms.ModelForm):

    my_new_field = forms.CharField(required=False)

    class Meta:
        model = Product
        # fields = '__all__'
        exclude = ('is_deleted', )
        widgets = {
            'condition': forms.widgets.RadioSelect(),
            'brand': forms.widgets.Select(attrs={'class': 'my_class'})
        }

    def clean_number_plate(self):
        reg = r'^\w{2}\d{4}\w{2}$'
        if not re.match(reg, self.cleaned_data['number_plate']):
            raise forms.ValidationError('Are you a monkey? Enter correct one!')
        return self.cleaned_data['number_plate']

    def save(self, commit=True):
        super(ProductCreateForm, self).save(commit)
        # if self.cleaned_data['my_new_field']:
        #     Tag.objects.create(
        #         name=self.cleaned_data['my_new_field']
        #     )