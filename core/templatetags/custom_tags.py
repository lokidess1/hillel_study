from django.template import Library
from django.contrib.auth.models import User


register = Library()


@register.simple_tag
def discr(a, b, c):
    return a + b * c


@register.inclusion_tag(filename="tags/user_list.html")
def user_list():
    users = User.objects.all()
    return {
        'users': users
    }
