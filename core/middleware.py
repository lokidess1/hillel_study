from django.utils.deprecation import MiddlewareMixin


class MyCustomMiddleware(MiddlewareMixin):

    def process_request(self, request):
        return request

    def process_response(self, request, response):
        return response
