from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.contrib.auth.models import AbstractUser


class CreatedUpdated(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Brand(CreatedUpdated):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


# TODO: Bad practice!!!
# class SubBrand(Brand):
#     sub_name = models.CharField(max_length=255)

# TODO: Good practice!!!
# class SubBrand(CreatedUpdated):
#     name = models.CharField(max_length=255)
#     sub_name = models.CharField(max_length=255)

class Tag(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


# class TagProduct(models.Model):
#     product = models.ForeignKey('Product', on_delete=models.CASCADE)
#     tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
#     click_count = models.IntegerField()


class ProductManager(models.Manager):

    def get_good(self):
        return self.get_queryset().filter(condition=Product.CONDITION_GOOD)

    def get_by_condition(self, condition):
        return self.get_queryset().filter(condition=condition)

    # def get_queryset(self):
    #     queryset = super(ProductManager, self).get_queryset()
    #     return queryset.filter(is_deleted=False)


class Product(CreatedUpdated):
    #  core_product
    CONDITION_NEW = 1
    CONDITION_GOOD = 2
    CONDITION_NORMAL = 3
    CONDITION_OMFG = 4

    CONDITION_CHOICES = (
        (CONDITION_NEW, 'New car (less then month in use)'),
        (CONDITION_GOOD, 'Good car (less then year in use)'),
        (CONDITION_NORMAL, 'Normal car (less then 5 years in use)'),
        (CONDITION_OMFG, 'OMFG (peace of crap)')
    )

    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True)
    run_km = models.IntegerField()
    condition = models.PositiveSmallIntegerField(choices=CONDITION_CHOICES)
    number_plate = models.CharField(max_length=8)
    tags = models.ManyToManyField(Tag, blank=True)  # through=TagProduct)
    is_deleted = models.BooleanField(default=False)

    objects = ProductManager()

    class Meta:
        # abstract = True
        db_table = 'product'
        ordering = ('id', 'created_at')
        unique_together = ('number_plate', 'brand')

    def __str__(self):
        return f"{self.number_plate}"

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        is_created = hasattr(self, 'pk')
        # pre_save
        super(Product, self).save(force_insert, force_update, using, update_fields)
        # post_save
        if is_created:
            # Code to send mail
            pass

#
# class MyAbstractController:
#
#     url_name = ...
#
#     def connection(self, ip, port):
#         raise NotImplemented
#
#
# class MyController(MyAbstractController):
#     pass
#


# @receiver(pre_save, sender=Product)
# def product_pre_save_action(sender, instance, **kwargs):
#     instance.run_km = 100


# class Profile(models.Model):
#     phone = models.CharField(max_length=11)
#     user = models.OneToOneField(User, on_delete=models.CASCADE)


class CustomUser(AbstractUser):
    phone = models.CharField(max_length=11)


@receiver(post_save, sender=Product)
def product_post_save_action(sender, instance, **kwargs):
    if kwargs['created']:
        # Code to send mail
        pass
