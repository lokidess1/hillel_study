import time
from hillel_study import celery_app


@celery_app.task
def my_super_task(a, b):  # TODO MAXIMUM SIMPLE DATA TYPES TO ARGUMENTS
    time.sleep(10)
    print(a + b)


@celery_app.task
def say_hello_task():
    print("Hello! You are greate!!!")
