from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Register your models here.
from core.forms import ProductCreateForm
from core.models import Brand, Product, Tag, CustomUser
from django.utils.translation import gettext, gettext_lazy as _
from django.utils.safestring import mark_safe


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {
            'fields': ('username', 'password')}),
        (_('Personal info'), {
            'fields': ('first_name', 'last_name', 'email', 'phone')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {
            'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2'),
        }),
    )


class ProductAdmin(admin.ModelAdmin):
    list_display = ('number_plate', 'brand_link', 'condition', 'is_deleted')
    search_fields = ('number_plate',)
    list_filter = ('condition', 'tags')
    actions = ('custom_delete', )
    # form = ProductCreateForm

    def brand_link(self, obj):
        return mark_safe(f"<a target='_blank' href='/backoffice/core/brand/{obj.brand_id}/change/'>{obj.brand.name}</a>")

    brand_link.short_description = "brand"
    brand_link.admin_order_field = 'brand'

    def custom_delete(self, request, queryset):
        queryset.update(is_deleted=True)
    custom_delete.short_description = 'Test'


class ProductInline(admin.TabularInline):
    model = Product
    extra = 0


class BrandAdmin(admin.ModelAdmin):
    inlines = [ProductInline]


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Tag)
