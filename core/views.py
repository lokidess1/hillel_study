from django.views.generic.base import View, TemplateView
from django.views.generic.edit import FormView, CreateView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.shortcuts import render
from django.db.models.query_utils import Q
from django.db.models import F, Sum, Avg, Count
from django.shortcuts import redirect


# class Index(View):
#
#     def get(self, request, pk):
#         context = {
#             'some_data': 123
#         }
#         return render(request, template_name='index.html', context=context)
from core.forms import ProductCreateForm
from core.models import Product, Brand
from django.contrib.auth.mixins import LoginRequiredMixin

from django.contrib.auth import get_user_model

# get_user_model() == settings.AUTH_USER_MODEL
from core.tasks import my_super_task


class Index(LoginRequiredMixin, TemplateView):
    template_name = 'index.html'

    def dispatch(self, request, *args, **kwargs):
        return super(Index, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        # self.request
        # self.kwargs['pk']
        context = super(Index, self).get_context_data(**kwargs)
        # products = Product.objects.filter(
        #     condition=Product.CONDITION_NEW,
        #     number_plate='XX1111XX'
        # )
        # SQL  number_plate LIKE "XX%"
        # products = Product.objects.filter(
        #     number_plate__istartswith='xx',
        #     run_km__range=(100, 200)
        # )
        # products = Product.objects.filter(
        #     tags__isnull=True
        # ).order_by('-condition')

        # brand = Brand.objects.get(name='Bently')

        # products = Product.objects.filter(
        #     brand__name='Bently'
        # )

        # products = Product.objects.all()
        # products = Product.objects.raw("SELECT * FROM product")
        # products = Product.objects.all().values('number_plate', 'brand__name')

        # products = Product.objects.all().values_list('run_km', flat=True)
        # print(products)

        # context['products'] = Product.objects.get_by_condition(Product.CONDITION_OMFG)
        #         # if 10 > 3:
        #         #     context['products'] = context['products'].filter(brand__name='Bently')
        # context['products'] = Product.objects.all().select_related('brand').prefetch_related('tags')

        # context['products'] = Product.objects.filter(
        #     # Q(brand_id=2) | Q(number_plate='XX2222XX') & Q(tags__isnull=False)
        #     # ~Q(brand_id=2)
        #     # Q(brand_id=1) | Q(number_plate='XX2222XX')
        # ).distinct().select_related('brand').prefetch_related('tags')

        # brand_names = Brand.objects.all().values_list('name', flat=True)
        # context['products'] = Product.objects.filter(
        #     brand__name=F('description')
        #     # brand__name__in=brand_names, description__in=brand_names
        # ).select_related('brand').prefetch_related('tags')

        # context['all_run'] = sum([x.run_km for x in Product.objects.all()])
        # context['all_run'] = Product.objects.all().aggregate(all_run=Avg('run_km'))['all_run']

        # brands = Brand.objects.all()
        # for brand in brands:
        #     brand.avg_run = Product.objects.filter(
        #         brand=brand
        #     ).aggregate(all_run=Avg('run_km'))['all_run']

        # context['brands'] = Brand.objects.all().annotate(avg_run=Avg('product__run_km')).annotate(
        #     product_count=Count('product')
        # ).filter(avg_run__gt=120)

        # TODO Forms

        context['form'] = ProductCreateForm()

        return context

    # def get(self, request, *args, **kwargs):
    #     pass
    #
    def post(self, request):
        form = ProductCreateForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
        context = self.get_context_data()
        context['form'] = form
        return self.render_to_response(context)


class ProductCreate(FormView):
    template_name = 'index.html'
    form_class = ProductCreateForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return super(ProductCreate, self).form_valid(form)
    
    def form_invalid(self, form):
        super(ProductCreate, self).form_invalid(form)


class ProductCreateFinal(CreateView):
    template_name = 'index.html'
    # form_class = ProductCreateForm
    model = Product
    fields = (
        'brand', 'description', 'run_km', 'number_plate', 'tags', 'condition'
    )
    # fields = '__all__'

    def get_initial(self):
        initial = super(ProductCreateFinal, self).get_initial()
        initial['description'] = "User dont add any description"
        return initial


class ProductList(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(ProductList, self).get_context_data(**kwargs)
        context['products'] = Product.objects.all()
        return context


class ProductDetail(DetailView):
    template_name = 'product_detail.html'
    model = Product


class ProductListView(ListView, CreateView):
    template_name = 'index.html'
    model = Product
    context_object_name = 'products'
    form_class = ProductCreateForm
    object = None

    def get_success_url(self):
        return '/'

    def get_queryset(self):
        my_super_task.delay(2, 2)
        return Product.objects.all().order_by('-id').select_related('brand')


class EditProductView(FormView):
    form_class = ProductCreateForm
    template_name = 'edit_product.html'

    def get_success_url(self):
        return f"/edit/{self.kwargs['id']}/"

    def get_form_kwargs(self):
        kwargs = super(EditProductView, self).get_form_kwargs()
        kwargs['instance'] = Product.objects.get(id=self.kwargs['id'])
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(EditProductView, self).form_valid(form)


class EditProduct(UpdateView):
    form_class = ProductCreateForm
    model = Product
    template_name = 'edit_product.html'
    pk_url_kwarg = 'id'

    def get_success_url(self):
        return f"/edit/{self.kwargs['id']}/"
