from core.forms import ProductCreateForm


def product_creation_form(request):
    return {
        'product_form': ProductCreateForm
    }
