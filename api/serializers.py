from rest_framework import serializers

from core.models import Product, Brand


class BrandSerializer(serializers.ModelSerializer):

    class Meta:
        model = Brand
        fields = ('id', 'name')


class ProductSerializer(serializers.ModelSerializer):

    brand_obj = BrandSerializer(read_only=True, source='brand')
    # brand = serializers.IntegerField(write_only=True)
    # price = serializers.
    tags = serializers.SerializerMethodField(read_only=True)
    condition_str = serializers.SerializerMethodField(read_only=True)
    condition = serializers.IntegerField(write_only=True)

    class Meta:
        model = Product
        fields = '__all__'

    def get_tags(self, obj):
        return list(obj.tags.all().values_list('name', flat=True))

    def get_condition_str(self, obj):
        return obj.get_condition_display()

    # def create(self, validated_data):
    #     self.validated_data
    #     pass

    # def update(self, instance, validated_data):
    #     pass

    # def validate(self, attrs):
    #     pass
    #
    # def validate_condition(self):
    #     raise serializers.ValidationError('asdasdasd')
