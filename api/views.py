from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework import mixins

from api.serializers import ProductSerializer
from core.models import Product


class ProductViewSet(mixins.ListModelMixin, GenericViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
