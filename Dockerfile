FROM python:3.6

WORKDIR /app
COPY . .
RUN mkdir collected_static

RUN pip install -r requirements.txt
